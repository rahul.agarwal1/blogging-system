from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post


def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'blog/home.html', context)

class PostListView(ListView):
    model = Post
    template_name='blog/home.html'
    context_object_name='posts'
   
class UserPostListView(ListView):
    model = Post
    template_name = 'blog/user_posts.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'posts'
    
class PostDetailView(DetailView):
    model = Post

class PostCreateView(CreateView):
    model = Post  
    fields= ['title', 'content'] 
    
class PostUpdateView(UpdateView):
    model = Post  
    fields= ['title', 'content']

def about(request):
    return render(request, 'blog/about.html', {'title': 'About'})